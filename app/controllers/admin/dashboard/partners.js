import Ember from 'ember';

export default Ember.Controller.extend({
    
    realGraphData: Ember.computed('model', function(){
        var names = this.get('model').mapBy('name');
        var online = this.get('model').mapBy('totalOnline');
        var offline = this.get('model').mapBy('totalOffline');
        var rGraphData = [];

        var i;
        for(i = 0; i < names.length; i++) {
         
            rGraphData.push({
                barLabel: names[i],
                value: online[i],
                sliceLabel: 'trained online'
            });

            rGraphData.push({
                barLabel: names[i],
                value: offline[i],
                sliceLabel: 'trained offline'
            });
        }

        return rGraphData;
    }),

    selectedSeedColor: 'rgb(56,66,169)',

    totalFemale: Ember.computed('model', function(){
        let sum = this.get('model').mapBy('totalFemale').reduce(function(a,b){ return a+b })
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }),

    totalMale: Ember.computed('model', function(){
        let sum = this.get('model').mapBy('totalMale').reduce(function(a,b){ return a+b })
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }),

    totalOnline: Ember.computed('model', function(){
        let sum = this.get('model').mapBy('totalOnline').reduce(function(a,b){ return a+b })
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }),

    totalOffline: Ember.computed('model', function(){
        let sum = this.get('model').mapBy('totalOffline').reduce(function(a,b){ return a+b })
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }),

    totalTrained: Ember.computed('model', function(){
        let sum = this.get('model').mapBy('totalTrained').reduce(function(a,b){ return a+b })
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }),

    breakdown: Ember.computed('model', function(){
        var names = this.get('model').mapBy('name');
        var trained = this.get('model').mapBy('totalTrained');
        var list = [];

        var i;
        for(i = 0; i < names.length; i++) {
            list.push({name: names[i], trained: trained[i]});
        }

        return list;
    })
});
