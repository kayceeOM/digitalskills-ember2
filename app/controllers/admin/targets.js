import Ember from 'ember';

export default Ember.Controller.extend({

	actions: {
		createTarget: function(){
			let data =  {
				quarter: this.get('quarter'),
				totalTrained: this.get('totalTrained'),
				year: new Date().getFullYear()
			}

		    const newTarget = this.get('store').createRecord('target', data);
		    newTarget.save()
		    		 .then(() => { 
		    		 	// console.log("saved target") 
    		 	   		this.set('quarter', '');
    		 	   		this.set('totalTrained', '');
    		 	   		this.set('year', '');
    		 	    }).catch((error) => {
    		 	    	// alert(error);
    		 	    	newTarget.deleteRecord();
                        this.set('quarter', '');
                        this.set('totalTrained', '');
                        this.set('year', '');
    		 	    });
		},

		updateTarget: function(target){
			target.save().then(() => { 
    		 	console.log("updated target") 
	 	    });
		},

		deleteTarget: function(target){
	        target.destroyRecord();
		}
	},

	realGraphData: Ember.computed('model', function(){
        var periods = this.get('compare-targets').mapBy('period');
        var expected = this.get('compare-targets').mapBy('expectedTrained');
        var actual = this.get('compare-targets').mapBy('actualTrained');
        var rGraphData = [];

        var i;
        for(i = 0; i < periods.length; i++) {
         
            rGraphData.push({
                group: periods[i],
                value: expected[i],
                label: 'expected trained'
            });

            rGraphData.push({
                group: periods[i],
                value: actual[i],
                label: 'actual trained'
            });
        }

        return rGraphData;
    }),

	selectedSeedColor: 'rgb(56,66,169)'
});
