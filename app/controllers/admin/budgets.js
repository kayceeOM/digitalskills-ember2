import Ember from 'ember';

export default Ember.Controller.extend({

	actions: {
		createBudget: function(){
			let data =  {
				quarter: this.get('quarter'),
				amount:  this.get('amount'),
				year:    new Date().getFullYear()
			}

		    const newBudget = this.get('store').createRecord('budget', data);

		    newBudget.save().then(() => { 
		    	console.log("saved budget")
	 	   		this.set('quarter', '');
	 	   		this.set('amount', '');
	 	   		this.set('year', '');
		     });
		},

		updateBudget: function(budget){
			budget.save().then(() => { 
    		 	console.log("updated target") 
	 	    });
		},

		deleteBudget: function(budget){
	        budget.destroyRecord();
		} 
	}

});
