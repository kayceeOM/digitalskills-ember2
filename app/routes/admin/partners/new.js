import Ember from 'ember';

export default Ember.Route.extend({
	
	actions: {
		createPartner: function(){
			const data = {
				name: this.controller.get('name'),
				email: this.controller.get('email'),
				password: 'password',
				password_confirmation: 'password'
			}

			const newPartner = this.get('store').createRecord('partner', data);
			newPartner.save().then(() => this.transitionTo('admin.partners'));

		},

		willTransition() {
	        this.controller.get('model').rollbackAttributes();
	    }
	}
});
