import Ember from 'ember';

export default Ember.Route.extend({
	model() {
	    return Ember.RSVP.hash({
	        targets: this.store.findAll('target'),
	        compareTargets: this.store.findAll('compare-target')
	    });
	},

	setupController(controller, model) {
	    this._super(...arguments);
	    Ember.set(controller, 'targets', model.targets);
	    Ember.set(controller, 'compare-targets', model.compareTargets);
	 }
});
