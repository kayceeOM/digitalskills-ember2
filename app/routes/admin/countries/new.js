import Ember from 'ember';

export default Ember.Route.extend({

	model() {
	    return this.get('store').createRecord('country');
	},

	actions: {
		createCountry: function(newCountry){
			newCountry.save().then(() => this.transitionTo('admin.countries'));
		},

		willTransition() {
	      // rollbackAttributes() removes the record from the store if the model 'isNew'
	      this.controller.get('model').rollbackAttributes();
	    }
	}
});
