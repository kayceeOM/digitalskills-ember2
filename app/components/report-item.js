import Ember from 'ember';

export default Ember.Component.extend({

	tagName: 'tr',
	classNames: ['report-item'],
	classNameBindings: ['isExpanded'],
	isExpanded: false,

	actions: {
	    toggleExpanded() {
	      this.toggleProperty('isExpanded');
	    }
	}
});
