import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
    location: config.locationType,
    rootURL: config.rootURL
});

Router.map(function() {
    this.route('login');

    this.route('reports', function() {
        this.route('new');
    });
    this.route('forecasts');

    this.route('admin-login', {path: '/'});

    this.route('admin', function() {
        this.route('dashboard', function(){
            this.route('countries');
            this.route('partners', {path: '/'});
        });

        this.route('reports');

        this.route('partners', function() {
            this.route('partner', { path: '/:partner_id' });
            this.route('new');
            this.route('edit', { path: 'edit/:partner_id' });
        });

        this.route('countries', function() {
            this.route('country', { path: '/:country_id' });
            this.route('new');
            this.route('edit', { path: 'edit/:country_id' });
        });

        this.route('targets');
        this.route('budgets');
    });
});

export default Router;
