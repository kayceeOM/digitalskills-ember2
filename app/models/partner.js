import DS from 'ember-data';

export default DS.Model.extend({
	
	trainings: 		DS.hasMany('training'),
	countries: 		DS.hasMany('country'),
    name:           DS.attr(),
    email:          DS.attr(),
    slug:           DS.attr(),
    logo: 			DS.attr()

});
