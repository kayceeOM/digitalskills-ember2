import DS from 'ember-data';

export default DS.Model.extend({
  period: DS.attr('string'),
  expectedTrained: DS.attr('number'),
  actualTrained: DS.attr('number')
});
