import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({

	partner:        DS.belongsTo('partner'),
	country:        DS.attr(),
	totalTrained:  DS.attr(),
	maleTrained:   DS.attr(),
	femaleTrained: DS.attr(),
	mode:           DS.attr(),
	status:         DS.attr(),
	trainingDate:  DS.attr(),
	description:    DS.attr(),
	snippet: Ember.computed('description', function() {
	    return `${this.get('description').substring(0,60) + ' ...'}`;
	})
	
});
