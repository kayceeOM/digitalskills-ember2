import DS from 'ember-data';

export default DS.Model.extend({
  name:     DS.attr('string'),
  totalTrained:     DS.attr('string'),
  totalOffline:     DS.attr('string'),
  totalOnline:     DS.attr('string'),
  totalPartners:     DS.attr('string'),
  slug:     DS.attr('string'),
  partners: DS.hasMany('partner')

});
