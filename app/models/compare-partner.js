import DS from 'ember-data';

export default DS.Model.extend({

	// url:   		  '/partners/compare',
    name:         DS.attr('string'),
    totalTrained: DS.attr('number'),
    totalOnline:  DS.attr('number'),
    totalOffline: DS.attr('number'),
    totalMale:    DS.attr('number'),
    totalFemale:  DS.attr('number')
});
