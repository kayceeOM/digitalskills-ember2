import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	
    name:         DS.attr('string'),
    totalTrained: DS.attr('number'),
    totalOnline:  DS.attr('number'),
    totalOffline: DS.attr('number'),
    totalMale:    DS.attr('number'),
    totalFemale:  DS.attr('number'),

});
